const sampleArray = [469, 755, 244, 245, 758, 450, 302,
    20, 712, 71, 456, 21, 398, 339, 882,
    848, 179, 535, 940, 472];



function katas1(){
    for(i = 1; i<=25;i++){ 
    let answer = i;
    let element = document.createElement("div");
    let d1 = document.getElementById("d1");
    d1.appendChild(element);
    let text = document.createTextNode(answer);
    element.appendChild(text); 
   
 
    }
}
katas1()




function katas2(){
    for(i = 25; i>=1;i--){
    answer = i;
    let element = document.createElement("div");
    let d1 = document.getElementById("d1");
    d1.appendChild(element);
    let text = document.createTextNode(answer);
    element.appendChild(text); 
    
    }
}

katas2()


function katas3(){
    for(i = -1; i>=-25;i--){
    answer = i;
    let element = document.createElement("div");
    let d1 = document.getElementById("d1");
    d1.appendChild(element);
    let text = document.createTextNode(answer);
    element.appendChild(text); 
    }
}


katas3()


function katas4(){
    for(i = -25; i<=-1;i++){
    answer = i;
    let element = document.createElement("div");
    let d1 = document.getElementById("d1");
    d1.appendChild(element);
    let text = document.createTextNode(answer);
    element.appendChild(text); 
    }
}


katas4()


function katas5(){
    for(i=25;i>=-25;i-=2){
    answer = i;
    let element = document.createElement("div");
    let d1 = document.getElementById("d1");
    d1.appendChild(element);
    let text = document.createTextNode(answer);
    element.appendChild(text); 
    }
}

katas5()


function katas6(){
    for(i = 0;i <=100;i+=3){
        answer = i;
        let element = document.createElement("div");
        let d1 = document.getElementById("d1");
        d1.appendChild(element);
        let text = document.createTextNode(answer);
        element.appendChild(text); 
    }
}

katas6()


function katas7(){
    for(i = 0;i <=100;i+=7){
        answer = i;
        let element = document.createElement("div");
        let d1 = document.getElementById("d1");
        d1.appendChild(element);
        let text = document.createTextNode(answer);
        element.appendChild(text); 
    }
}

katas7()


function katas8(){
    for(i=100;i>=1;i--){
        if(i % 3 ===0 || i % 7 ===0){
        answer = i;
        let element = document.createElement("div");
        let d1 = document.getElementById("d1");
        d1.appendChild(element);
        let text = document.createTextNode(answer);
        element.appendChild(text); 
        }
    }     
    
}

katas8()


function katas9(){
    for(i = 5;i <=100;i+=5){
        if (i % 2 !== 0 )
        answer = i;
        let element = document.createElement("div");
        let d1 = document.getElementById("d1");
        d1.appendChild(element);
        let text = document.createTextNode(answer);
        element.appendChild(text); 
    }
}
katas9()


function katas10(){
let element = document.createElement("div")
let d1 = document.getElementById("d1");
d1.appendChild(element);
let text = document.createTextNode(sampleArray);
element.appendChild(text); 

}
katas10()


function katas11(){
    for(i = 0; i<sampleArray.length; i++){
        if(sampleArray[i] % 2 === 0){
        let element = document.createElement("div")
        let d1 = document.getElementById("d1");
        d1.appendChild(element);
        let text = document.createTextNode(sampleArray[i]);
        element.appendChild(text); 
        }
    }
}
katas11()

function katas12(){
    for(i = 0; i<sampleArray.length; i++){
        if(sampleArray[i] % 2 !== 0){
        let element = document.createElement("div")
        let d1 = document.getElementById("d1");
        d1.appendChild(element);
        let text = document.createTextNode(sampleArray[i]);
        element.appendChild(text); 
        }
    }
}

katas12()


function katas13(){
    for(i = 0; i<sampleArray.length; i++){
        if(sampleArray[i] % 8 === 0){
        let element = document.createElement("div")
        let d1 = document.getElementById("d1");
        d1.appendChild(element);
        let text = document.createTextNode(sampleArray[i]);
        element.appendChild(text); 
        }
    }
}

katas13()

function katas14(){
    for (i=0;i<sampleArray.length;i++){
        let answer = sampleArray[i] * sampleArray[i];
        let element = document.createElement("div")
        let d1 = document.getElementById("d1");
        d1.appendChild(element);
        let text = document.createTextNode(answer);
        element.appendChild(text); 

    }
}

katas14()

function katas15(){
    let sum = 0;
    for(i=1;i<=20;i++){
       sum += i; 
    }
    
    let element = document.createElement("div")
    let d1 = document.getElementById("d1");
    d1.appendChild(element);
    let text = document.createTextNode(sum);
    element.appendChild(text); 
}
katas15()

function katas16(){
    let sum = 0;
    for (i=0;i<sampleArray.length;i++){
        sum += sampleArray[i];
    }
        let element = document.createElement("div")
        let d1 = document.getElementById("d1");
        d1.appendChild(element);
        let text = document.createTextNode(sum);
        element.appendChild(text); 

}
katas16()

function katas17(){
    let answer = Math.min(...sampleArray)
    let element = document.createElement("div")
    let d1 = document.getElementById("d1");
    d1.appendChild(element);
    let text = document.createTextNode(answer);
    element.appendChild(text); 
}

katas17()

function katas18(){
    let answer = Math.max(...sampleArray)
    let element = document.createElement("div")
    let d1 = document.getElementById("d1");
    d1.appendChild(element);
    let text = document.createTextNode(answer);
    element.appendChild(text); 
}

katas18()